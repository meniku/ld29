extends Node

var current_scene = null

var current_level = -1
var levels = [
	"res://level_tut.scn", 
	"res://level_edge_pipes.scn",
	"res://level_walls.scn",
	"res://level_shifter.scn",
	"res://level_1.scn",
	"res://level_2.scn",
	"res://level_3.scn",
	"res://level_4.scn",
]

var actions_this_level = 0
var actions_total = 0
var hud = null
var load_level = false
var is_dragging = false
var sample_player = null
#var is_hovering = null
const connector_size = Vector2(4, 4)

func has_more_levels():
	return current_level < levels.size() - 1

func _ready():
	var root = get_scene().get_root()
	current_scene = root.get_child( root.get_child_count() -1 )
	sample_player = ResourceLoader.load("res://sample_player.scn").instance()
	
#	show_hud()
	
func show_hud():
	var s = ResourceLoader.load("res://hud.scn")
	hud = s.instance()
	current_scene.add_child(hud)
	update_hud()
	
	
func show_finish():
	var s = ResourceLoader.load("res://finish.scn")
	var finish = s.instance()
	current_scene.add_child(finish)
	
func play_sound(sound_name):
	if sample_player:
		sample_player.play(sound_name);
	
	
func next_level():
	actions_this_level = 0
	current_level = current_level + 1
	if current_level < levels.size():
		goto_scene(levels[current_level])
	else:
		print("game completed")
		OS.get_main_loop().quit()

func goto_scene(scene):
	print(current_scene.get_path())
	var s = ResourceLoader.load(scene)
	current_scene.queue_free()
	load_level = true
	current_scene = s.instance()
	get_scene().get_root().add_child(current_scene)
	show_hud()

func get_level(): 
	return get_scene().get_nodes_in_group("level")[0]

func inc_steps(): 
	actions_this_level = actions_this_level + 1
	actions_total = actions_total + 1
	update_hud()
	
func update_hud():
	if(hud):
		hud.get_node("txtSteps").set_text(str(actions_this_level))
		hud.get_node("txtStepsTotal").set_text(str(actions_total))
	

func update_all_connectors() : 
	var all_pipes = get_scene().get_nodes_in_group("pipe")
	var finished = true
	for pipe in all_pipes:
		pipe.is_connected = is_connected(pipe)
		if(!pipe.is_connected):
			finished = false
	
	if(!is_connected(get_level().get_node("start")) || !is_connected(get_level().get_node("goal"))):
		finished = false
	
	if finished: 
		finish()

func is_connected(pipe):
	if(!pipe):
		print("PIPE IS NULL !!!")
		return null
	var children = pipe.get_children()
	var is_connected = true
	for child in children:
		if(child.is_in_group("connector")):
			if get_intersecting_connector(child) ==  null:
				is_connected = false
				
	return is_connected
	
func get_intersecting_connector(from_connector):
	var all_connectors = get_scene().get_nodes_in_group("connector")
	var rect1 = Rect2( from_connector.get_global_pos() - connector_size*0.5, connector_size )
	for connector in all_connectors:
		if connector != from_connector:
			var rect2 = Rect2( connector.get_global_pos() - connector_size*0.5, connector_size )
			if rect1.intersects(rect2):
				return connector
	return null
	
func get_first_connector(pipe): 
	var children = pipe.get_children()
	for child in children:
		if(child.is_in_group("connector")):
			return child
	return null

func get_other_connector(connector): 
	var children = connector.get_parent().get_children()
	for child in children:
		if(child.is_in_group("connector") && child != connector):
			return child
	return null

var all_bubbles
var finished_bubbles
var accum
var bubble_index

func finish() :
	print("finished")
	finished_bubbles = 0
	get_level().get_node("start/valve").turn()
	var all_pipes = get_scene().get_nodes_in_group("pipe")
	for pipe in all_pipes:
		pipe.disable()

	all_bubbles = get_scene().get_nodes_in_group("bubble")
	bubble_index = 0
	accum = 0.0
	set_process(true)
	#start_next_bubble()

func _process(delta):
	accum += delta
	if(accum > 0.15):
		start_next_bubble()
		accum = 0
	
func start_next_bubble():
	if(all_bubbles.size() > bubble_index):
		all_bubbles[bubble_index].move_to_finish()
		bubble_index = bubble_index + 1

func bubble_finished():
	finished_bubbles = finished_bubbles + 1
	if finished_bubbles == all_bubbles.size():
		set_process(false)
		show_finish()