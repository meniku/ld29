
extends Node2D

export var frame_length = 0.2
export var move_speed = 400
export var threshold = 6.0

var sprite
var rect
var accum = 0
var start_x

var pipe_begin_pos
var pipe_end_pos
var last_connector
var current_waypoint = -1
var waypoints
var sound1_played = false
var sound2_played = false
var global = null

func _ready():
	set_process(true)
	sprite = get_node("Sprite")
	rect = sprite.get_region_rect()
	start_x = rect.pos.x
	accum = randf()
	global = get_node("/root/global")
	pass

func _process(delta):
	# doing this earlier will return wrong position in level_walls.scn
	if pipe_begin_pos == null:
		pipe_begin_pos = global.get_level().get_node("start/pipe_begin").get_global_pos()
	if pipe_end_pos == null:
		pipe_end_pos = global.get_level().get_node("goal/pipe_end").get_global_pos()
	var global_pos = get_global_pos()
	update_animation(delta, global_pos)
	update_position(delta, global_pos)

func update_animation(delta, global_pos):
	accum += delta
	if(accum > frame_length * 3):
		accum = 0
		rect.pos.x = start_x + 0
	elif(accum > frame_length * 2):
		rect.pos.x = start_x + 64
	elif(accum > frame_length):
		rect.pos.x = start_x + 32
	
	if(global_pos.y < pipe_begin_pos.y && global_pos.y > pipe_end_pos.y): 
		if !sound1_played:
			sound1_played = true
			global.play_sound("bubble_1")
		rect.pos.y = 224
	else:
		rect.pos.y = 192
		if !sound2_played &&  global_pos.y < pipe_end_pos.y:
			sound2_played = true
			global.play_sound("bubble_2")
		
	sprite.set_region_rect(rect)

func update_position(delta, global_pos) : 
	if(current_waypoint > -1): 
		var target_pos = waypoints[current_waypoint]
		if (global_pos - target_pos).length() < threshold:
			if current_waypoint < waypoints.size() - 1:
				current_waypoint = current_waypoint + 1
			else:
				current_waypoint = -2
				global.bubble_finished()
		else:
			set_pos(get_pos() + Vector2(delta, delta) * (target_pos - global_pos).normalized() * Vector2(move_speed, move_speed))

	
func move_to_finish():
	var goal_node = global.get_level().get_node("goal")
	waypoints = []
	var waypoint_node = global.get_first_connector(global.get_level().get_node("start"))
	waypoints.append(waypoint_node.get_global_pos())
	
	while waypoint_node != goal_node:
		waypoint_node = global.get_other_connector(global.get_intersecting_connector(waypoint_node))
		if(!waypoint_node): #goal reached
			waypoint_node = goal_node
		var middle_point = waypoint_node.get_parent().get_node("waypoint")
		if(middle_point):
			print("append waypoint")
			waypoints.append(middle_point.get_global_pos())
		waypoints.append(waypoint_node.get_global_pos())
		
	var goal_pos = waypoints[waypoints.size()-1]
	waypoints.append(Vector2(goal_pos.x - 20 + 40 * randf(), goal_pos.y - 20 + 40 * randf()))
		
	current_waypoint = 0