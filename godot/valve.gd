
extends Node2D

export var frame_length = 0.1
export var times = 3

var sprite
var rect
var accum = 0
var start_x 
var turn_no = 0

func _ready():
	sprite = get_node("Sprite")
	rect = sprite.get_region_rect()
	start_x = rect.pos.x

func _process(delta):
	accum += delta
	if(accum > frame_length * 3):
		accum = 0
		rect.pos.x = start_x + 0
		turn_no = turn_no + 1
		if turn_no >= times:
			set_process(false)
	elif(accum > frame_length * 2):
		rect.pos.x = start_x + 64
	elif(accum > frame_length):
		rect.pos.x = start_x + 32
	sprite.set_region_rect(rect)
	
func turn() : 
	set_process(true)
	get_node("SamplePlayer").play("valve")