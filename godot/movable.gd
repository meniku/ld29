extends KinematicBody2D

export var allow_move_horizontal = true
export var allow_move_vertical = true
export var size = Vector2(32, 32)
export var pull_sound = "pipe_3"
export var drop_sound = "pipe_2"

var dragging = false
var drag_started_mpos
var drag_started_pos
var xyfactors = Vector2(1, 1)
var last_target = null
var x_checked = false
var y_checked = false
var right_checked = false
var is_connected = false
var global

func _ready():
	set_process_input(true)
	if(!allow_move_horizontal):
		xyfactors.x = 0
	if(!allow_move_vertical):
		xyfactors.y = 0
	global = get_node("/root/global")
	is_connected = global.is_connected(self)
	show_or_hide_hover()
	pass

func _input(event):
	if (event.type == InputEvent.MOUSE_BUTTON):
		if(!event.pressed):
			if dragging:
				dragging = false
				global.is_dragging = false
				x_checked = false
				y_checked = false
				last_target = null
				show_or_hide_hover()
				snap_to_grid()
				global.play_sound(drop_sound)
				global.inc_steps()
				global.update_all_connectors()
		else:
			if !global.is_dragging:
				dragging = is_mouse_hovered()
				if dragging:
					global.is_dragging = true
					global.play_sound(pull_sound)
					drag_started_mpos = Input.get_mouse_pos ( )
					drag_started_pos = get_pos()
					show_dragging()
	
	elif (event.type == InputEvent.MOUSE_MOTION):
		if( dragging ) :
			var mpos = Input.get_mouse_pos( )
			var offset = (mpos - drag_started_mpos)
			var target = drag_started_pos + offset * xyfactors
			
			if last_target != null && last_target != get_pos() && !x_checked:
				# help moving at horizontal walls
				x_checked = true
				move_to(Vector2(last_target.x, get_pos().y))
			elif last_target != null && last_target != get_pos() && !y_checked:
				# help moving at vertical walls
				y_checked = true
				move_to(Vector2(get_pos().x, last_target.y))
			else: 
				# normal movement
				move_to(target)
				x_checked = false
				y_checked = false
				
			last_target = target
		else:
			if(!global.is_dragging):
				show_or_hide_hover()

func snap_to_grid() :
	var pos = get_pos()
	set_pos(Vector2(align(pos.x), align(pos.y)))
	
func align(value) :
	var rest = int(value) % 16
	if(rest > 8):
		return value + 16 - rest
	else:
		return value- rest

func _fixed_process(delta):
	var space = get_world_2d().get_space()
	var space_state = Physics2DServer.space_get_direct_state( space )

func is_mouse_hovered() :
	var rect = Rect2( get_pos() - size*0.5, size )
	return (rect.has_point(Input.get_mouse_pos ( )))
	
func show_or_hide_hover() : 
	if is_mouse_hovered():
		show_hover()
	else:
		hide_hover()
	
func show_dragging() : 
	var factor = 1.0
#	if(! is_connected ) : 
#		factor = 1.5
	get_node("Sprite").set_modulate(Color(factor * 1.5, factor * 1.5, factor * 1.5))
	set_opacity(1.0)
	
func show_hover() : 
	var factor = 1.0
#	if(! is_connected ) : 
#		factor = 1.5
	get_node("Sprite").set_modulate(Color(factor * 1.25, factor * 1.25, factor * 1.25))
	set_opacity(1.0)

func hide_hover() : 
	var factor = 1.0
#	if(! is_connected ) : 
#		factor = 1.5
	get_node("Sprite").set_modulate(Color(factor * 1.0, factor * 1.0, factor * 1.0))
	set_opacity(1.0)
	
func disable() : 
	set_process_input(false)
	hide_hover()
