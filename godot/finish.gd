
extends Particles2D

# member variables here, example:
# var a=2
# var b="textvar"

func _ready():
	var global = get_node("/root/global")
	if(global.has_more_levels()):
		get_node("Panel/Label").set_text(str("Level ", global.current_level + 1, " completed in ", global.actions_this_level, " actions"))
	else:
		get_node("Panel/Label").set_text(str("Congratulations, you completed Breathless in ", global.actions_total, " actions"))
		get_node("Panel/Button").set_text(str("Quit Breathless"))
	pass

func _on_Button_pressed():
	get_node("/root/global").next_level()
